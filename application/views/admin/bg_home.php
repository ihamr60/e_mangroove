<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>E-Mangroove</title>
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->

    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
    <?php $this->load->view('admin/style'); ?>
    <!-- end: MAIN CSS -->

    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->

    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>

<body>
    
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->

                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->

            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12 hidden-sm hidden-xs">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="clip-home-3"></i>
                                <a href="<?php echo base_url() ?>index.php/web">
                                    Home
                                </a>
                            </li>
                            <li class="active">
                                Dashboard
                            </li>
                            
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <br/><br/>
                <div class="row">
                    <?php echo $this->session->flashdata('info'); ?>

                    <p align="center"><img src="<?php echo base_url(); ?>vendor/assets/images/web/langsa.png" width="300px" ></p><br>
                    <h2 align="center">Setoran Hari ini:<br><?php echo "Rp" . number_format($income['income'],0,',','.');  ?></h2>
                   
                    <p align="center">Menu Utama masih dalam pengembangan, <br>di atas merupakan nilai yang harus <br>disetorkan ke perusahaan.</p>
                    <!-- ISI KONTEN DISINI -->
                  <!--  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div> -->



                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; Developed by <a target="_blank" href="http://digisoft.co.id">Digisoft Technology</a>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>

    <script src="<?php echo base_url(); ?>vendor/code/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>vendor/code/modules/series-label.js"></script>
    <script src="<?php echo base_url(); ?>vendor/code/modules/exporting.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <?php echo $menu_atas; ?>

    <?php /*
        $label = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
         
        for($bulan = 1;$bulan < 13;$bulan++)
        {

            $data       = array();
            $data2      = array();
            $masuk      = $this->db->query("select sum(nominal) as jumlah_pemasukan from tbl_keuangan where MONTH(created_at)='$bulan' AND YEAR(created_at)='".date('Y')."' AND jenis_transaksi='Pemasukan'");
            $keluar     = $this->db->query("select sum(nominal) as jumlah_pengeluaran from tbl_keuangan where MONTH(created_at)='$bulan' AND YEAR(created_at)='".date('Y')."' AND jenis_transaksi='Pengeluaran'");

                if ($masuk->num_rows() > 0)
                    {
                        $data = $masuk->row_array();
                    }

                if ($keluar->num_rows() > 0)
                    {
                        $data2 = $keluar->row_array();
                    }

            $masuk->free_result();
            $keluar->free_result();
            $jumlah_masuk[]=$data['jumlah_pemasukan'];
            $jumlah_keluar[]=$data2['jumlah_pengeluaran'];
        }

        // GRAFIK JUMLAH PENGELUARAN PER TAHUN
            $data_tot_msk   = array();
            $data_tot_klr   = array();
            $total_masuk    = $this->db->query("select sum(nominal) as total_masuk from tbl_keuangan where YEAR(created_at)='".date('Y')."' AND jenis_transaksi='Pemasukan'");
            $total_keluar   = $this->db->query("select sum(nominal) as total_keluar from tbl_keuangan where YEAR(created_at)='".date('Y')."' AND jenis_transaksi='Pengeluaran'");

                if ($total_masuk->num_rows() > 0)
                    {
                        $data_tot_msk = $total_masuk->row_array();
                    }

                if ($total_keluar->num_rows() > 0)
                    {
                        $data_tot_klr = $total_keluar->row_array();
                    }

            $total_masuk->free_result();
            $total_keluar->free_result();
    */
    ?>

    <?php //echo json_encode($jumlah_masuk); ?>
    <script type="text/javascript">
        Highcharts.chart('container', {
            title: {
                text: '<b>GRAFIK PENGELUARAN & PEMASUKAN TAHUN <?php echo date('Y') ?></b>'
            },
            xAxis: {
                categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
            },
            labels: {
                items: [{
                    html: 'Total Jumlah Nominal',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                    }
                }]
            },
            series: [{
                type: 'column',
                name: 'Pemasukan',
                color: Highcharts.getOptions().colors[7],
                data: [
                        <?php
                            for($bulan = 0;$bulan < 12; $bulan++)
                            {
                                if($jumlah_masuk[$bulan]==null)
                                {
                                    echo "0,";
                                }
                                else
                                {
                                    echo $jumlah_masuk[$bulan];
                                    echo ",";
                                }
                            }
                        ?>
                      ]
            }, {
                type: 'column',
                name: 'Pengeluaran',
                color: Highcharts.getOptions().colors[5],
                data: [
                        <?php
                            for($bulan = 0;$bulan < 12; $bulan++)
                            {
                                if($jumlah_keluar[$bulan]==null)
                                {
                                    echo "0,";
                                }
                                else
                                {
                                    echo $jumlah_keluar[$bulan];
                                    echo ",";
                                }
                            }
                        ?>
                      ]
            }, {
                type: 'pie',
                name: 'Jumlah Nominal',
                data: [{
                    name: 'Pemasukan Tahun <?php echo date('Y') ?>',
                    y: <?php echo $data_tot_msk['total_masuk'] ?>,
                    color: Highcharts.getOptions().colors[7] 
                }, {
                    name: 'Pengeluaran Tahun <?php echo date('Y') ?>',
                    y: <?php echo $data_tot_klr['total_keluar'] ?>,
                    color: Highcharts.getOptions().colors[5] 
                }],
                center: [100, 80],
                size: 100,
                showInLegend: false,
                dataLabels: {
                    enabled: false
                }
            }]
        });
    </script>

    <script>
        jQuery(document).ready(function() {
            Main.init();
            Index.init();
        });
    </script>   

</body>

</html>