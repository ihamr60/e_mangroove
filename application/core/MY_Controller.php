<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	/**
	 * Created by ilham.r@multidatasistem.co.id
	 */
	
	public function get_id()
	{
		$result = $this->db->query("Select UPPER(REPLACE(UUID(), '-', '')) as Id");
		return ($result->num_rows() == 1) ? $result->result()[0]->Id : FALSE;
	}

	//  ID GRUP TELEGRAM AKUN ROBOT
	public function get_id_group_telegram()
	{
		// return "-329371519"; // GROUP AKUN ROBOT
		 return "885296637"; // MY ID BOT TELEGRAM
		 //return "-1001468865598";
	}

	// TOKEN TELEGRAM BOT
	public function get_token_bot_telegram()
	{
		 return "802249814:AAFbpkwVP5NLtRzI13iwCx5cU4PH02CDQhg";
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */