<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	/**
	 * Created by ilham.r@multidatasistem.co.id
	 */

	public function index(){
		$cek = $this->session->userdata('logged_in');
		if(empty($cek)){
			$this->load->view('login');
		}
		else{
			$st = $this->session->userdata('stts');
			if($st=='mangroove officer'){
				header('location:'.base_url().'index.php/admin');
			}
		}
	}

	public function login(){
		$u = $this->input->post('username');
		$p = $this->input->post('password');
		$this->web_app_model->getLoginData($u, $p);
	}

	public function logout(){
		$cek = $this->session->userdata('logged_in');
		if(empty($cek)){
			header('location:'.base_url().'index.php/web');
		}
		else{
			$this->session->sess_destroy(); // memusnahkan sessionnya
			header('location:'.base_url().'index.php/web');
		}
	}
}
