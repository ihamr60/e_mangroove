<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {
	
	/**
	 * Created by ilham.r@multidatasistem.co.id
	 */

// START BG_INDEX

	public function index()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='mangroove officer')
		{
			$bc['pengeluaran']		= $this->web_app_model->getPengeluaran();
			$bc['income']			= $this->web_app_model->getSetoran();
			$bc['name'] 			= $this->session->userdata('name');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['header'] 			= 'MANGROOVE - <b>HOME</b>';
			$bc['menu_atas'] 		= $this->load->view('admin/menu_atas',$bc,true);
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	
			$this->load->view('admin/bg_home',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

// EN BG_INDEX

	public function bg_pengeluaran()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='mangroove officer')
		{
			date_default_timezone_set('Asia/Jakarta');
			$bc['data_pengeluaran']	= $this->web_app_model->get2WhereAllItemOrderLimit('pengeluaran','kategori',date('Y-m-d'),'created_at','mangroove_income','no','DESC','10');	
			$bc['name'] 			= $this->session->userdata('name');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['header'] 			= 'MANGROOVE - <b>PAYMENT</b>';
			$bc['menu_atas'] 		= $this->load->view('admin/menu_atas',$bc,true);
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	
			$this->load->view('admin/bg_pengeluaran',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_pemasukan_karcis()
	{
		$cek = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='mangroove officer')
		{
			date_default_timezone_set('Asia/Jakarta');
			$bc['maxKarcis'] 		= $this->web_app_model->maxKarcis();
			$bc['data_karcis']		= $this->web_app_model->get2WhereAllItemOrderLimit('karcis','kategori',date('Y-m-d'),'created_at','mangroove_income','no','DESC','10');	
			$bc['data_tarif']		= $this->web_app_model->getWhereOneItem('1','no','mangroove_tarif');
			$bc['name'] 			= $this->session->userdata('name');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['header'] 			= 'MANGROOVE - <b>KARCIS</b>';
			$bc['menu_atas'] 		= $this->load->view('admin/menu_atas',$bc,true);
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	
			$this->load->view('admin/bg_pemasukan_karcis',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function add_single_karcis()
	{
		$maxKarcis 					= $this->web_app_model->maxKarcis();
		date_default_timezone_set('Asia/Jakarta');
		$created_at					= date('Y-m-d');
		$timed_at					= date('H:i:s');
		$kategori					= 'Karcis';

		$no_karcis					= $maxKarcis;
		$jumlah 					= $this->input->post('jumlah');
		$debit 						= $this->input->post('debit');
		$pic 						= $this->input->post('pic');
		$ket 						= 'Single Karcis';

		$data = array(		
			'created_at' 			=> $created_at,
			'timed_at' 				=> $timed_at,
			'kategori' 				=> $kategori,
			'no_karcis'				=> $no_karcis,
			'jumlah'				=> $jumlah,
			'debit'					=> $debit,
			'pic'					=> $pic,
			'ket'					=> $ket,
			);
		
		$this->web_app_model->insertData($data,'mangroove_income');
		header('location:'.base_url().'index.php/admin/bg_pemasukan_karcis');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success!
													</strong>
													Transaksi Pemesanan SINGLE KARCIS <b>($no_karcis)</b> Berhasil !
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Great!!',
										                text:  'No Karcis $no_karcis berhasil dipesan!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
											");

		$income						= $this->web_app_model->getSetoran();

		date_default_timezone_set('Asia/Jakarta');   
		$time 	= date('l, d-m-Y | H:i');
		$time_jam 	= date('H:i');
		$time_tgl 	= date('d F Y');

		$TOKEN  = $this->get_token_bot_telegram();  // ganti token ini dengan token bot mu
		$chatid = $this->get_id_group_telegram(); // ini id saya di telegram @hasanudinhs silakan diganti dan disesuaikan
		$pesan 	= "<b>[KARCIS MANGROOVE TERJUAL]</b>\n".$time." WIB\n\n1 Karcis terjual ($no_karcis), total setoran ke perusahaan saat ini menjadi: <b>Rp".number_format($income['income'],0,',','.')."</b>\n\nPIC:\n".$pic."\nMangroove Officer";

		// ----------- code -------------

		$method	= "sendMessage";
		$url    = "https://api.telegram.org/bot" . $TOKEN . "/". $method;
		$post = [
		 'chat_id' => $chatid,
		  'parse_mode' => 'HTML', // aktifkan ini jika ingin menggunakan format type HTML, bisa juga diganti menjadi Markdown
		 'text' => $pesan
		];

		$header = [
		 "X-Requested-With: XMLHttpRequest",
		 "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36" 
		];

		// hapus 1 baris ini:
		//die('Hapus baris ini sebelum bisa berjalan, terimakasih.');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		//curl_setopt($ch, CURLOPT_REFERER, $refer);
		//curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post );   
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$datas = curl_exec($ch);
		$error = curl_error($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$debug['text'] = $pesan;
		$debug['code'] = $status;
		$debug['status'] = $error;
		$debug['respon'] = json_decode($datas, true); 
	}

	public function add_multiple_karcis()
	{
		$maxKarcis 					= $this->web_app_model->maxKarcis();
		date_default_timezone_set('Asia/Jakarta');
		$created_at					= date('Y-m-d');
		$timed_at					= date('H:i:s');
		$kategori					= 'Karcis';

		
		$jumlah 					= $this->input->post('jumlah');
		$no_karcis_awal				= $maxKarcis;
		$no_karcis_akhir			= ($no_karcis_awal-1)+$jumlah;
		$debit 						= $this->input->post('debit')*$jumlah;
		$pic 						= $this->input->post('pic');
		$ket 						= $this->input->post('ket');

		$data = array(		
			'created_at' 			=> $created_at,
			'timed_at' 				=> $timed_at,
			'kategori' 				=> $kategori,
			'no_karcis'				=> $no_karcis_akhir,
			'jumlah'				=> $jumlah,
			'debit'					=> $debit,
			'pic'					=> $pic,
			'ket'					=> $ket,
			);
		
		$this->web_app_model->insertData($data,'mangroove_income');
		header('location:'.base_url().'index.php/admin/bg_pemasukan_karcis');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success!
													</strong>
													Transaksi Pemesanan MULTIPLE KARCIS $jumlah buah <b>($no_karcis_awal - $no_karcis_akhir)</b> Berhasil !
												</p>
											</div>");
		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Great!!',
										                text:  '$jumlah buah No Karcis $no_karcis_awal - $no_karcis_akhir berhasil dipesan!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");

		$income						= $this->web_app_model->getSetoran();

		date_default_timezone_set('Asia/Jakarta');   
		$time 	= date('l, d-m-Y | H:i');
		$time_jam 	= date('H:i');
		$time_tgl 	= date('d F Y');

		$TOKEN  = $this->get_token_bot_telegram();  // ganti token ini dengan token bot mu
		$chatid = $this->get_id_group_telegram(); // ini id saya di telegram @hasanudinhs silakan diganti dan disesuaikan
		$pesan 	= "<b>[KARCIS MANGROOVE TERJUAL]</b>\n".$time." WIB\n\n$jumlah Karcis terjual ($no_karcis_awal - $no_karcis_akhir), total setoran ke perusahaan saat ini menjadi: <b>Rp".number_format($income['income'],0,',','.')."</b>\n\nPIC:\n".$pic."\nMangroove Officer";

		// ----------- code -------------

		$method	= "sendMessage";
		$url    = "https://api.telegram.org/bot" . $TOKEN . "/". $method;
		$post = [
		 'chat_id' => $chatid,
		  'parse_mode' => 'HTML', // aktifkan ini jika ingin menggunakan format type HTML, bisa juga diganti menjadi Markdown
		 'text' => $pesan
		];

		$header = [
		 "X-Requested-With: XMLHttpRequest",
		 "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36" 
		];

		// hapus 1 baris ini:
		//die('Hapus baris ini sebelum bisa berjalan, terimakasih.');


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		//curl_setopt($ch, CURLOPT_REFERER, $refer);
		//curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post );   
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$datas = curl_exec($ch);
		$error = curl_error($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$debug['text'] = $pesan;
		$debug['code'] = $status;
		$debug['status'] = $error;
		$debug['respon'] = json_decode($datas, true);
	}

	public function add_pengeluaran()
	{
		//$maxKarcis 					= $this->web_app_model->maxKarcis();
		date_default_timezone_set('Asia/Jakarta');
		$created_at					= date('Y-m-d');
		$timed_at					= date('H:i:s');
		$kategori					= 'Pengeluaran';

		
		$jumlah 					= '';
		$no_karcis					= '';
		$angka 						= $this->input->post('nominal');
		$kredit						= str_replace(".", "", $angka);
		$pic 						= $this->input->post('pic');
		$ket 						= $this->input->post('ket');

		$data = array(		
			'created_at' 			=> $created_at,
			'timed_at' 				=> $timed_at,
			'kategori' 				=> $kategori,
			'no_karcis'				=> $no_karcis,
			'jumlah'				=> $jumlah,
			'kredit'				=> $kredit,
			'pic'					=> $pic,
			'ket'					=> $ket,
			);
		
		$this->web_app_model->insertData($data,'mangroove_income');
		header('location:'.base_url().'index.php/admin/bg_pengeluaran');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success!
													</strong>
													Transaksi pengeluaran $ket berhasil dilaporkan!
												</p>
											</div>");
		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Transkasi pengeluaran berhasil dilaporkan!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");

		$income						= $this->web_app_model->getSetoran();

		date_default_timezone_set('Asia/Jakarta');   
		$time 	= date('l, d-m-Y | H:i');
		$time_jam 	= date('H:i');
		$time_tgl 	= date('d F Y');

		$TOKEN  = $this->get_token_bot_telegram();  // ganti token ini dengan token bot mu
		$chatid = $this->get_id_group_telegram(); // ini id saya di telegram @hasanudinhs silakan diganti dan disesuaikan
		$pesan 	= "<b>[PENGELUARAN MANGROOVE]</b>\n".$time." WIB\n\nTerjadi transaksi pengeluaran senilai Rp. ".number_format($kredit,0,',','.')." yang digunakan dengan,\n\nKeterangan: $ket, \n\nSehingga total setoran ke perusahaan saat ini menjadi: <b>Rp".number_format($income['income'],0,',','.')."</b>\n\nPIC:\n".$pic."\nMangroove Officer";

		// ----------- code -------------

		$method	= "sendMessage";
		$url    = "https://api.telegram.org/bot" . $TOKEN . "/". $method;
		$post = [
		 'chat_id' => $chatid,
		  'parse_mode' => 'HTML', // aktifkan ini jika ingin menggunakan format type HTML, bisa juga diganti menjadi Markdown
		 'text' => $pesan
		];

		$header = [
		 "X-Requested-With: XMLHttpRequest",
		 "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36" 
		];

		// hapus 1 baris ini:
		//die('Hapus baris ini sebelum bisa berjalan, terimakasih.');


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		//curl_setopt($ch, CURLOPT_REFERER, $refer);
		//curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post );   
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$datas = curl_exec($ch);
		$error = curl_error($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$debug['text'] = $pesan;
		$debug['code'] = $status;
		$debug['status'] = $error;
		$debug['respon'] = json_decode($datas, true);
	}
}