<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>E-Mangroove</title>
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->

    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />

    <!-- end: MAIN CSS -->

    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />

    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" />   
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" /> 


    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/assets/plugin/bootstrap-timepicker.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/jquery.tagsinput/dist/jquery.tagsinput.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/summernote/dist/summernote.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <?php $this->load->view('admin/style'); ?>

</head>

<body>
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
            <?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->

                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->

            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12 hidden-sm hidden-xs">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="clip-home-3"></i>
                                <a href="<?php echo base_url() ?>index.php/web">
                                    Home
                                </a>
                            </li>
                            <li class="active">
                                Pergudangan (Keluar masuk barang)
                            </li>
                            <li class="search-box">
                                <form class="sidebar-search">
                                    <div class="form-group">
                                        <input type="text" placeholder="Start Searching...">
                                        <button class="submit">
                                            <i class="clip-search-3"></i>
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <div class="row">
                    <div class="col-sm-7 col-md-12">
                        
                        <?php date_default_timezone_set('Asia/Jakarta'); ?>  <hr>                      
                        <h4 align="center">Today: <?php echo date('d F Y')?></h4>
                     

                           
                            
                            <div class="panel-body">
                                <div class="alert alert-info">
                                    <button data-dismiss="alert" class="close">
                                        &times;
                                    </button>
                                    <i class="fa fa-info-circle"></i>
                                    <strong>Info!</strong> Hai <?php echo $name ?> <b>Fitur Karcis</b> adalah fitur transaksi pemasukan meliputi pembelian karcis pengunjung. Setiap pembelian karcis harus di input dan terdata ke aplikasi.
                                </div>
                                <?php echo $this->session->flashdata('info'); ?>
                                <div class="row">
                                    <form method="post" action="<?php echo base_url() ?>index.php/admin/add_single_karcis">
                                       
                                        <?php date_default_timezone_set('Asia/Jakarta'); ?>
                                        
                                        <div class="col-md-12">
                                            <h4 align="center">NOMOR SERI KARCIS SELANJUTNYA:</h4> <h1 align="center"><b><?php echo $maxKarcis ?></b></h1>
                                            <p>
                                                <input
                                                    type="hidden" 
                                                    name="no_karcis"
                                                    value="<?php echo $maxKarcis; ?>"
                                                    class="form-control"
                                                    readonly
                                                    required>

                                                <input
                                                    type="hidden" 
                                                    name="jumlah"
                                                    value="1"
                                                    class="form-control"
                                                    readonly
                                                    required>

                                                <input
                                                    type="hidden" 
                                                    name="debit"
                                                    value="<?php echo $data_tarif['karcis'] ?>"
                                                    class="form-control"
                                                    readonly
                                                    required>

                                                <input
                                                    type="hidden" 
                                                    name="pic"
                                                    value="<?php echo $name ?>"
                                                    class="form-control"
                                                    readonly
                                                    required>
                                            </p>    
                                        </div>
                                        <br/>
                                        <div align="center" class="col-md-12 space20">
                                            <button id="myButton" data-loading-text="Loading..." class="demo btn btn-blue" type="submit">
                                              <i class="fa fa-plus"></i>  Pesan 1 Karcis 
                                           </button>
                                           <a data-toggle="modal" class="demo btn btn-teal" href="#order">
                                               Pesan >1 Karcis  <i class="fa fa-plus"></i>
                                           </a>
                                        </div> 
                                    </form>
                                   <hr>
                                </div>
                                <div id="panel_projects" class="tab-pane table-responsive"> <!-- table-responsive  -->
                                    <table class="table table-striped table-bordered table-hover" id="__sample-table-2">
                                        <thead>
                                            <tr>
                                                <th class="col-to-export center">Last Karcis</th>
                                                <th class="col-to-export center">Tanggal</th>
                                                <th width="100px" class="center">Tarif</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php 
                                                foreach($data_karcis as $a)
                                                {
                                           ?>
                                        <tr>
                                            <td class="col-to-export">
                                                <?php echo $a->no_karcis ?>
                                            </td>
                                            <td class="col-to-export">
                                                <?php echo date('d-m-Y', strtotime($a->created_at)).' '.date('H:s', strtotime($a->timed_at)) ?>
                                            </td>
                                            <td class="center currency-mask">
                                                <?php echo "Rp " . number_format($a->debit,0,',','.'); ?>
                                            </td>
                                        </tr>
                                            <?php } ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                       </div>
                   </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <div id="order" class="modal fade" tabindex="-1" data-width="1060" style="display: none;">
                <form role="form" action="<?php echo base_url();?>index.php/admin/add_multiple_karcis/" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">Pesan > 1 Ticket Parkir:</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Jumlah Karcis dipesan:</label>
                             
                                    <div class="input-group">
                                        <span class="input-group-addon"> KARCIS </span>
                                        <input 
                                            type="number" 
                                            placeholder="Just Number" 
                                            required name="jumlah" 
                                            class="form-control">

                                        <input
                                            type="hidden" 
                                            name="no_karcis"
                                            value="<?php echo $maxKarcis; ?>"
                                            class="form-control"
                                            readonly
                                            required>

                                        <input
                                            type="hidden" 
                                            name="debit"
                                            value="<?php echo $data_tarif['karcis'] ?>"
                                            class="form-control"
                                            readonly
                                            required>

                                        <input
                                            type="hidden" 
                                            name="pic"
                                            value="<?php echo $name ?>"
                                            class="form-control"
                                            readonly
                                            required>
                                    </div>
                            </div>
                            <br/>
                            <div class="col-md-12">
                                <p>
                                    <textarea 
                                        placeholder="Keterangan" 
                                        type="text" 
                                        name="ket" 
                                        class="form-control"
                                        ></textarea>
                                </p> 
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                            Close
                        </button>
                        <button id="myButton2" data-loading-text="Loading..." type="submit" class="btn btn-blue">
                            Pesan Karcis
                        </button>
                    </div>
                </form>
            </div>

            <div id="modal_filtering" class="modal fade" tabindex="-1" data-width="730" style="display: none;">
                <form role="form" action="<?php echo base_url();?>index.php/admin/filter_gudang/" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">Tampilkan data gudang berdasarkan tanggal</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    Date Range Picker
                                </p>
                                <div class="input-group">
                                    <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    <input type="text" name="range_tgl" class="form-control date-range">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                            Close
                        </button>
                        <button type="submit" class="btn btn-blue">
                            Process
                        </button>
                    </div>
                </form>
            </div>

        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; Developed by <a target="_blank" href="http://digisoft.co.id">Digisoft Technology</a>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->

    <!-- JAVASCRIPT REQUIRED -->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/jquery.mask.min.js"></script> 
    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>


    <script src="<?php echo base_url();?>vendor/bower_components/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/autosize/dist/autosize.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/summernote/dist/summernote.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-fileinput/js/fileinput.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/form-elements.min.js"></script>
    <!-- END JAVASCRIPT REQUIRED -->

    <?php echo $menu_atas; ?>
    <?php echo $this->session->flashdata('info2'); ?>

    <script>
        jQuery(document).ready(function() {
            Main.init();
            TableExport.init();
            FormElements.init();
            UIModals.init();
        });
    </script>   

    <script>
      $('#myButton').on('click', function () {
        var $btn = $(this).button('loading')
        // business logic...
        $btn.button('loading')
      })

      $('#myButton2').on('click', function () {
        var $btn = $(this).button('loading')
        // business logic...
        $btn.button('loading')
      })
    </script>

</body>

</html>