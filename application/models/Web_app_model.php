<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_App_Model extends CI_Model 
{	

	/**
	 * Created by ilham.r@multidatasistem.co.id
	 */

	public function getLoginData($usr, $psw){
		$u = $usr; 
		$p = md5($psw);

		$q_cek_login = $this->db->get_where('tbl_login', array('username' => $u, 'password' => $p));
		
		if(count($q_cek_login->result())>0){
			
			foreach ($q_cek_login->result() as $qck){			
				
				// LEVEL 1 ADMIN
				if($qck->stts=='mangroove officer'){
					
					$q_ambil_data = $this->db->get_where('mangroove_officer', array('username' => $u));
					
					foreach($q_ambil_data -> result() as $qad){
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->username;
						$sess_data['name'] 		 	 = $qad->nama_lengkap;
						$sess_data['stts'] 			 = 'mangroove officer';
					    $this->session->set_userdata($sess_data);
					}
					
					header('location:'.base_url().'index.php/admin');
				}
			}
		}
		else{
			
			header('location:'.base_url().'index.php/web');
			$this->session->set_flashdata('info','<div class="alert alert-danger">
                                <button data-dismiss="alert" class="close">
                                    &times;
                                </button>
                                <i class="fa fa-times-circle"></i>
                                <strong>Ups, Maaf!</strong> Username atau password anda salah.
                            </div>');
		}
	}	

	/*public function getDataKarcisHarini()
	{
		$this->db->query('SELECT * mangroove_income WHERE created_at(date)');
	} */

	public function getSetoran()
	{	
		$data = array();
		//return $this->db->query("SELECT SUM(debit)-SUM(kredit) FROM $tbl");
		$Q = $this->db->query("SELECT SUM(debit)-SUM(kredit) AS income FROM mangroove_income");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getPengeluaran()
	{	
		$data = array();
		//return $this->db->query("SELECT SUM(debit)-SUM(kredit) FROM $tbl");
		$Q = $this->db->query("SELECT SUM(kredit) AS pengeluaran FROM mangroove_income WHERE kategori='Pengeluaran'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getAllData($table)
	{
		return $this->db->get($table)->result();
	}

	public function getWhereAllItemOrder($where,$field,$table,$field_order, $order)
	{
		$this->db->where($field,$where);
		$this->db->order_by($field_order, $order);
		return $this->db->get($table)->result();
	}

	public function get2WhereAllItemOrderLimit($where,$field,$where2,$field2,$table,$field_order, $order, $limit)
	{
		$this->db->where($field,$where);
		$this->db->limit($limit);
		$this->db->where($field2,$where2);
		$this->db->order_by($field_order, $order);
		return $this->db->get($table)->result();
	}

	public function insertData($data,$table)
	{
		$this->db->insert($table,$data);
	}

	public function deleteData($table,$data)
	{
		$this->db->delete($table, $data);
	}

	public function getJoinWhere($idTabel1,$idTabel2,$table1,$table2,$field,$where)
	{
		  $this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		  $this->db->from($table1);
		  $this->db->where($field,$where);
		  $query = $this->db->get();
		  return $query->result();
	}

	
	public function getWhereOneItem($where,$field,$table)
	{
		$data = array();
  		$options = array($field => $where);
  		$Q = $this->db->get_where($table,$options);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getWhereAllItem($where,$field,$table)
	{
		$this->db->where($field,$where);
		return $this->db->get($table)->result();
	}

	public function maxKarcis()
	{
		$bulan	 = date('n');
		$tahun	 = date('Y'); 
		$dataMax = $this->db->query("SELECT MAX(no_karcis) as ID FROM mangroove_income WHERE kategori='Karcis'");
		$row     = $dataMax->row_array();
		//$nomor	 = "/".$bulan."/".$tahun;
		
		if($row['ID']=="")
		{
			$ID = "000001";
		}
		else
		{
			$MaksID = $row['ID'];
			$MaksID++;
			if($MaksID < 10) $ID = "00000".$MaksID;
			else if($MaksID < 100) $ID = "0000".$MaksID;
			else if($MaksID < 1000) $ID = "000".$MaksID;
			else if($MaksID < 10000) $ID = "00".$MaksID;
			else if($MaksID < 100000) $ID = "0".$MaksID;
			else $ID = $MaksID;
		}
		
		return $ID;
	}
}
