-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 21, 2020 at 03:01 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `palma_group`
--

-- --------------------------------------------------------

--
-- Table structure for table `mangroove_income`
--

CREATE TABLE `mangroove_income` (
  `no` int(11) NOT NULL,
  `no_karcis` int(11) NOT NULL COMMENT 'IF Pemasukan Karcis',
  `created_at` date NOT NULL,
  `timed_at` time NOT NULL,
  `pic` varchar(30) NOT NULL,
  `kategori` varchar(20) NOT NULL COMMENT 'Karcis, Setoran, Pengeluaran',
  `jumlah` int(11) NOT NULL,
  `debit` int(11) NOT NULL,
  `kredit` int(11) NOT NULL,
  `ket` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mangroove_income`
--

INSERT INTO `mangroove_income` (`no`, `no_karcis`, `created_at`, `timed_at`, `pic`, `kategori`, `jumlah`, `debit`, `kredit`, `ket`) VALUES
(639, 1, '2020-02-19', '11:35:43', 'Ilham Ramadhan', 'Setoran', 1, 0, 17600000, 'Single Karcis'),
(640, 2, '2020-02-19', '11:35:51', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(641, 202, '2020-02-19', '11:36:01', 'Ilham Ramadhan', 'Karcis', 200, 1000000, 0, ''),
(642, 203, '2020-02-19', '11:36:18', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(643, 204, '2020-02-19', '11:36:22', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(644, 205, '2020-02-19', '11:36:26', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(645, 206, '2020-02-19', '11:36:30', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(646, 207, '2020-02-19', '11:36:38', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(647, 208, '2020-02-19', '11:36:46', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(648, 209, '2020-02-19', '11:36:53', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(649, 210, '2020-02-19', '11:36:57', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(650, 211, '2020-02-19', '11:37:01', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(651, 212, '2020-02-19', '11:38:11', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(652, 213, '2020-02-19', '11:38:18', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(653, 214, '2020-02-19', '11:38:27', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(654, 215, '2020-02-19', '11:41:47', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(655, 215, '2020-02-19', '11:41:57', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(656, 216, '2020-02-19', '11:42:15', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(657, 217, '2020-02-19', '11:42:21', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(658, 218, '2020-02-19', '11:42:28', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(659, 219, '2020-02-19', '11:42:32', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(660, 220, '2020-02-19', '11:42:51', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(661, 215, '2020-02-19', '11:42:51', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(662, 221, '2020-02-19', '11:42:56', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(663, 222, '2020-02-19', '11:43:03', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(664, 223, '2020-02-19', '11:43:08', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(665, 224, '2020-02-19', '11:43:13', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(666, 225, '2020-02-19', '11:43:17', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(667, 226, '2020-02-19', '11:43:22', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(668, 227, '2020-02-19', '11:43:27', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(669, 225, '2020-02-19', '11:43:31', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(670, 228, '2020-02-19', '11:43:37', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(671, 229, '2020-02-19', '11:43:40', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(672, 230, '2020-02-19', '11:43:57', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(673, 231, '2020-02-19', '11:44:03', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(674, 232, '2020-02-19', '11:44:46', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(675, 233, '2020-02-19', '11:44:53', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(676, 234, '2020-02-19', '11:44:58', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(677, 235, '2020-02-19', '11:45:01', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(678, 236, '2020-02-19', '11:45:05', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(679, 237, '2020-02-19', '11:45:09', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(680, 238, '2020-02-19', '11:45:14', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(681, 438, '2020-02-19', '11:45:26', 'Ilham Ramadhan', 'Karcis', 200, 1000000, 0, ''),
(682, 228, '2020-02-19', '11:45:30', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(683, 439, '2020-02-19', '11:45:35', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(684, 440, '2020-02-19', '11:45:39', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(685, 441, '2020-02-19', '11:45:43', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(686, 442, '2020-02-19', '11:45:48', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(687, 443, '2020-02-19', '11:46:05', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(688, 444, '2020-02-19', '11:46:15', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(689, 3444, '2020-02-19', '11:46:24', 'Ilham Ramadhan', 'Karcis', 3000, 15000000, 0, ''),
(690, 3445, '2020-02-19', '11:46:32', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(691, 3446, '2020-02-19', '11:46:37', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(692, 3447, '2020-02-19', '11:46:43', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(693, 3448, '2020-02-19', '11:46:48', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(694, 3449, '2020-02-19', '11:46:54', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(695, 3450, '2020-02-19', '11:47:01', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(696, 3451, '2020-02-19', '11:47:06', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(697, 3452, '2020-02-19', '11:47:11', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(698, 3453, '2020-02-19', '11:47:17', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(699, 3454, '2020-02-19', '11:47:27', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(700, 3455, '2020-02-19', '11:47:32', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(701, 3475, '2020-02-19', '11:47:42', 'Ilham Ramadhan', 'Karcis', 20, 100000, 0, ''),
(702, 3476, '2020-02-19', '11:47:46', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(703, 3477, '2020-02-19', '11:47:54', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(704, 232, '2020-02-19', '11:48:03', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(705, 3478, '2020-02-19', '11:48:08', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(706, 3479, '2020-02-19', '11:48:13', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(707, 3480, '2020-02-19', '11:48:20', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(708, 3478, '2020-02-19', '11:48:24', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(709, 3481, '2020-02-19', '11:48:24', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(710, 3482, '2020-02-19', '11:48:30', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(711, 3482, '2020-02-19', '11:48:32', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(712, 3483, '2020-02-19', '11:49:25', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(713, 3484, '2020-02-19', '11:49:34', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(714, 3485, '2020-02-19', '11:50:15', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(715, 3505, '2020-02-19', '11:50:23', 'Ilham Ramadhan', 'Karcis', 20, 100000, 0, ''),
(716, 3506, '2020-02-19', '11:50:27', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(717, 3507, '2020-02-19', '11:50:32', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(718, 3508, '2020-02-19', '11:50:42', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(719, 3509, '2020-02-19', '11:51:23', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(720, 3510, '2020-02-19', '11:51:28', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(721, 3511, '2020-02-19', '11:51:32', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(722, 3512, '2020-02-19', '11:51:36', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(723, 3513, '2020-02-19', '14:34:43', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(724, 3514, '2020-02-19', '14:34:49', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(725, 3515, '2020-02-19', '15:11:26', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(726, 3565, '2020-02-19', '15:20:50', 'Ilham Ramadhan', 'Karcis', 50, 250000, 0, ''),
(727, 3566, '2020-02-19', '15:24:34', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(728, 3567, '2020-02-19', '15:25:27', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(729, 0, '2020-02-19', '16:15:20', 'Ilham Ramadhan', 'Setoran', 0, 265000, 0, 'Setoran dibayar ke perusahaan'),
(730, 0, '2020-02-19', '16:15:45', 'Ilham Ramadhan', 'Setoran', 0, 0, 530000, 'Setoran dibayar ke perusahaan'),
(731, 0, '2020-02-19', '16:36:01', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'Setoran dibayar ke perusahaan'),
(732, 0, '2020-02-19', '16:36:25', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'Setoran dibayar ke perusahaan'),
(733, 0, '2020-02-19', '16:36:49', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'Setoran dibayar ke perusahaan'),
(734, 0, '2020-02-19', '16:37:32', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'Setoran dibayar ke perusahaan'),
(735, 0, '2020-02-19', '16:37:42', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'Setoran dibayar ke perusahaan'),
(736, 0, '2020-02-19', '16:37:48', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'Setoran dibayar ke perusahaan'),
(737, 3568, '2020-02-19', '16:40:30', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(738, 3569, '2020-02-19', '16:40:37', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(739, 3570, '2020-02-19', '16:40:47', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(740, 3571, '2020-02-19', '16:41:02', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(741, 3771, '2020-02-19', '16:41:26', 'Ilham Ramadhan', 'Karcis', 200, 1000000, 0, 'Rombongan usu menanam'),
(742, 3772, '2020-02-19', '16:41:43', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(743, 3774, '2020-02-19', '16:41:51', 'Ilham Ramadhan', 'Karcis', 2, 10000, 0, ''),
(744, 3775, '2020-02-19', '16:41:59', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(745, 3776, '2020-02-19', '16:42:05', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(746, 3777, '2020-02-19', '16:42:10', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(747, 3800, '2020-02-19', '16:42:24', 'Ilham Ramadhan', 'Karcis', 23, 115000, 0, ''),
(748, 3801, '2020-02-19', '16:42:33', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(749, 3802, '2020-02-19', '16:42:51', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(750, 3803, '2020-02-19', '16:43:00', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(751, 3828, '2020-02-19', '16:43:11', 'Ilham Ramadhan', 'Karcis', 25, 125000, 0, ''),
(752, 3829, '2020-02-19', '16:43:31', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(753, 3830, '2020-02-19', '16:43:34', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(754, 3836, '2020-02-19', '16:43:40', 'Ilham Ramadhan', 'Karcis', 6, 30000, 0, ''),
(755, 3837, '2020-02-19', '16:43:43', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(756, 3829, '2020-02-19', '16:43:48', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(757, 3838, '2020-02-19', '16:43:53', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(758, 3839, '2020-02-19', '16:43:57', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(759, 3840, '2020-02-19', '16:44:01', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(760, 3841, '2020-02-19', '16:44:05', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(761, 3842, '2020-02-19', '16:44:09', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(762, 3843, '2020-02-19', '16:44:14', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(763, 3844, '2020-02-19', '16:44:22', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(764, 3845, '2020-02-19', '16:44:28', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(765, 3850, '2020-02-19', '16:44:38', 'Ilham Ramadhan', 'Karcis', 5, 25000, 0, ''),
(766, 3851, '2020-02-19', '16:44:47', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(767, 3853, '2020-02-19', '16:44:55', 'Ilham Ramadhan', 'Karcis', 2, 10000, 0, ''),
(768, 3854, '2020-02-19', '16:45:00', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(769, 3857, '2020-02-19', '16:45:11', 'Ilham Ramadhan', 'Karcis', 3, 15000, 0, ''),
(770, 3858, '2020-02-19', '16:45:16', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(771, 0, '2020-02-19', '16:45:27', 'Ilham Ramadhan', 'Setoran', 0, 0, 1460000, 'Setoran dibayar ke perusahaan'),
(772, 3859, '2020-02-19', '16:46:18', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(773, 4359, '2020-02-19', '16:46:30', 'Ilham Ramadhan', 'Karcis', 500, 2500000, 0, ''),
(774, 4360, '2020-02-19', '16:46:40', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(775, 4361, '2020-02-19', '16:46:47', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(776, 4362, '2020-02-19', '16:46:55', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(777, 4363, '2020-02-19', '16:47:00', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(778, 4364, '2020-02-19', '16:47:04', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(779, 4365, '2020-02-19', '16:57:50', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(780, 4366, '2020-02-19', '16:58:11', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(781, 4367, '2020-02-19', '16:58:26', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(782, 4368, '2020-02-19', '16:58:32', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(783, 4369, '2020-02-19', '16:58:41', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(784, 0, '2020-02-19', '17:00:23', 'Ilham Ramadhan', 'Setoran', 0, 0, 2555000, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(785, 0, '2020-02-19', '17:00:31', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(786, 0, '2020-02-19', '17:01:32', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(787, 0, '2020-02-19', '17:09:34', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(788, 0, '2020-02-19', '17:09:39', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(789, 4370, '2020-02-19', '17:09:49', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(790, 4371, '2020-02-19', '17:09:53', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(791, 4372, '2020-02-19', '17:09:58', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(792, 4373, '2020-02-19', '17:10:09', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(793, 4374, '2020-02-19', '17:10:16', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(794, 4375, '2020-02-19', '17:10:23', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(795, 4377, '2020-02-19', '17:10:31', 'Ilham Ramadhan', 'Karcis', 2, 10000, 0, ''),
(796, 4378, '2020-02-19', '17:10:36', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(797, 3838, '2020-02-19', '17:10:44', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(798, 4379, '2020-02-19', '17:10:51', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(799, 4380, '2020-02-19', '17:10:58', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(800, 0, '2020-02-19', '17:11:17', 'Ilham Ramadhan', 'Setoran', 0, 0, 60000, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(801, 0, '2020-02-19', '19:03:53', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(802, 0, '2020-02-19', '19:04:00', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(803, 4381, '2020-02-20', '09:16:21', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(804, 4401, '2020-02-20', '09:16:33', 'Ilham Ramadhan', 'Karcis', 20, 100000, 0, ''),
(805, 4402, '2020-02-20', '09:16:47', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(806, 4403, '2020-02-20', '09:16:53', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(807, 4404, '2020-02-20', '09:17:21', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(808, 0, '2020-02-20', '09:17:31', 'Ilham Ramadhan', 'Setoran', 0, 0, 120000, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(809, 4405, '2020-02-20', '09:24:12', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(810, 0, '2020-02-20', '09:24:19', 'Ilham Ramadhan', 'Setoran', 0, 0, 5000, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(811, 4406, '2020-02-20', '09:31:48', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(812, 4407, '2020-02-20', '09:32:01', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(813, 4408, '2020-02-20', '09:32:42', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(814, 4409, '2020-02-20', '09:33:37', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(815, 4410, '2020-02-20', '09:33:54', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(816, 4411, '2020-02-20', '09:34:09', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(817, 4461, '2020-02-20', '09:34:20', 'Ilham Ramadhan', 'Karcis', 50, 250000, 0, ''),
(818, 4462, '2020-02-20', '09:35:07', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(819, 4463, '2020-02-20', '09:35:21', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(820, 4464, '2020-02-20', '09:35:40', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(821, 4492, '2020-02-20', '09:36:36', 'Ilham Ramadhan', 'Karcis', 28, 140000, 0, ''),
(822, 4512, '2020-02-20', '09:37:28', 'Ilham Ramadhan', 'Karcis', 20, 100000, 0, ''),
(823, 4513, '2020-02-20', '09:38:16', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(824, 4523, '2020-02-20', '09:38:39', 'Ilham Ramadhan', 'Karcis', 10, 50000, 0, ''),
(825, 4524, '2020-02-20', '09:39:53', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(826, 4525, '2020-02-20', '09:40:01', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(827, 4526, '2020-02-20', '09:40:14', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(828, 4527, '2020-02-20', '09:40:57', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(829, 4528, '2020-02-20', '09:41:02', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(830, 4548, '2020-02-20', '09:41:17', 'Ilham Ramadhan', 'Karcis', 20, 100000, 0, ''),
(831, 4549, '2020-02-20', '09:41:22', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(832, 4550, '2020-02-20', '09:42:12', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(833, 4551, '2020-02-20', '09:42:23', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(834, 0, '2020-02-20', '09:45:11', 'Ilham Ramadhan', 'Setoran', 0, 0, 730000, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(835, 4552, '2020-02-20', '09:48:24', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(836, 0, '2020-02-20', '09:49:17', 'Ilham Ramadhan', 'Setoran', 0, 0, 5000, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(837, 4553, '2020-02-20', '09:59:15', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(838, 4554, '2020-02-20', '09:59:20', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(839, 0, '2020-02-20', '10:23:53', 'Ilham Ramadhan', 'Setoran', 0, 0, 10000, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(840, 0, '2020-02-20', '10:24:05', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(841, 0, '2020-02-20', '10:24:45', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(842, 0, '2020-02-20', '10:24:49', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(843, 0, '2020-02-20', '10:25:33', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(844, 0, '2020-02-20', '10:27:02', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(845, 0, '2020-02-20', '10:27:39', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(846, 0, '2020-02-20', '10:27:53', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(847, 4552, '2020-02-20', '10:28:03', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(848, 4555, '2020-02-20', '10:28:07', 'Ilham Ramadhan', 'Karcis', 1, 50000, 0, 'Single Karcis'),
(849, 4556, '2020-02-20', '10:28:17', 'Ilham Ramadhan', 'Karcis', 1, 50000, 0, 'Single Karcis'),
(850, 4557, '2020-02-20', '10:28:22', 'Ilham Ramadhan', 'Karcis', 1, 50000, 0, 'Single Karcis'),
(851, 4555, '2020-02-20', '10:28:30', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(852, 4559, '2020-02-20', '10:28:36', 'Ilham Ramadhan', 'Karcis', 2, 100000, 0, ''),
(853, 0, '2020-02-20', '10:28:40', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(854, 0, '2020-02-20', '10:29:30', 'Ilham Ramadhan', 'Setoran', 0, 0, 260000, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(855, 0, '2020-02-20', '10:30:07', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(856, 0, '2020-02-20', '10:31:16', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'Setoran dibayar ke perusahaan'),
(857, 0, '2020-02-20', '10:31:20', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'Setoran dibayar ke perusahaan'),
(858, 0, '2020-02-20', '10:31:57', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(859, 0, '2020-02-20', '10:32:19', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(860, 0, '2020-02-20', '10:32:42', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(861, 0, '2020-02-20', '10:33:08', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(862, 0, '2020-02-20', '10:33:12', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(863, 0, '2020-02-20', '10:34:08', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(864, 0, '2020-02-20', '10:34:28', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(865, 4558, '2020-02-20', '10:35:22', 'Ilham Ramadhan', 'Karcis', 1, 50000, 0, 'Single Karcis'),
(866, 4560, '2020-02-20', '10:35:26', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(867, 4561, '2020-02-20', '10:35:32', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(868, 4562, '2020-02-20', '10:35:36', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(869, 0, '2020-02-20', '10:35:54', 'Ilham Ramadhan', 'Setoran', 0, 0, 65000, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(870, 0, '2020-02-20', '10:36:48', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(871, 0, '2020-02-20', '10:37:33', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(872, 0, '2020-02-20', '10:38:33', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(873, 0, '2020-02-20', '10:40:36', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(874, 0, '2020-02-20', '10:41:06', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(875, 0, '2020-02-20', '10:41:33', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(876, 0, '2020-02-20', '10:42:17', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(877, 4563, '2020-02-20', '10:44:04', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(878, 4564, '2020-02-20', '10:44:08', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(879, 4584, '2020-02-20', '10:44:15', 'Ilham Ramadhan', 'Karcis', 20, 100000, 0, ''),
(880, 4585, '2020-02-20', '10:44:20', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(881, 4586, '2020-02-20', '10:44:30', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(882, 4606, '2020-02-20', '10:44:37', 'Ilham Ramadhan', 'Karcis', 20, 100000, 0, ''),
(883, 4607, '2020-02-20', '10:45:03', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(884, 4619, '2020-02-20', '10:45:18', 'Ilham Ramadhan', 'Karcis', 12, 60000, 0, ''),
(885, 4620, '2020-02-20', '10:45:28', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(886, 0, '2020-02-20', '10:50:35', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(887, 0, '2020-02-20', '10:50:44', 'Ilham Ramadhan', 'Setoran', 0, 0, 290000, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(888, 0, '2020-02-20', '10:52:09', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(889, 0, '2020-02-20', '10:59:18', 'Ilham Ramadhan', 'Setoran', 0, 0, 0, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(890, 4621, '2020-02-20', '11:00:23', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(891, 4622, '2020-02-20', '11:00:30', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(892, 4623, '2020-02-20', '11:00:37', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(893, 4624, '2020-02-20', '11:00:46', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(894, 4621, '2020-02-20', '11:19:01', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(895, 4625, '2020-02-20', '11:19:18', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(896, 4626, '2020-02-20', '11:19:23', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(897, 4646, '2020-02-20', '11:19:29', 'Ilham Ramadhan', 'Karcis', 20, 100000, 0, ''),
(898, 4666, '2020-02-20', '11:20:03', 'Ilham Ramadhan', 'Karcis', 20, 100000, 0, ''),
(899, 4667, '2020-02-20', '11:21:51', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(900, 4668, '2020-02-20', '11:31:42', 'Putri Balqis', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(901, 4668, '2020-02-20', '11:32:18', 'Ilham Ramadhan', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(902, 4669, '2020-02-20', '11:32:51', 'Ilham Ramadhan', 'Karcis', 2, 10000, 0, ''),
(903, 4670, '2020-02-20', '14:27:29', 'Putri Balqis', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(904, 4671, '2020-02-20', '14:30:37', 'Putri Balqis', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(905, 4672, '2020-02-20', '14:30:42', 'Putri Balqis', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(906, 4673, '2020-02-20', '14:32:26', 'Putri Balqis', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(907, 4674, '2020-02-20', '14:32:32', 'Putri Balqis', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(908, 4675, '2020-02-20', '14:33:56', 'Putri Balqis', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(909, 4676, '2020-02-20', '14:34:47', 'Putri Balqis', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(910, 4677, '2020-02-20', '14:35:04', 'Putri Balqis', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(911, 4678, '2020-02-20', '15:11:43', 'Ilham', 'Karcis', 1, 0, 0, 'Single Karcis'),
(912, 4679, '2020-02-20', '15:12:01', 'Ilham', 'Karcis', 1, 0, 0, 'Single Karcis'),
(913, 4680, '2020-02-20', '15:12:19', 'Ilham', 'Karcis', 1, 0, 0, 'Single Karcis'),
(914, 4681, '2020-02-20', '15:12:23', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(915, 4678, '2020-02-20', '15:14:33', 'Putri Balqis', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(916, 4720, '2020-02-20', '15:14:41', 'Putri Balqis', 'Karcis', 39, 195000, 0, ''),
(917, 4721, '2020-02-20', '15:14:48', 'Putri Balqis', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(918, 4722, '2020-02-20', '15:14:59', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(919, 0, '2020-02-20', '15:15:14', 'Ilham Ramadhan', 'Setoran', 0, 0, 515000, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(920, 4723, '2020-02-20', '15:19:23', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(921, 4724, '2020-02-20', '15:19:43', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(922, 4725, '2020-02-20', '15:19:47', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(923, 4748, '2020-02-20', '15:20:03', 'Ilham', 'Karcis', 23, 115000, 0, 'Rombongan kuala simpang '),
(924, 4749, '2020-02-20', '15:21:26', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(925, 4750, '2020-02-20', '15:23:56', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(926, 4751, '2020-02-20', '15:24:09', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(927, 4752, '2020-02-20', '15:54:01', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(928, 4753, '2020-02-20', '15:56:36', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(929, 4754, '2020-02-20', '15:56:48', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(930, 4755, '2020-02-20', '15:56:57', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(931, 4756, '2020-02-20', '15:57:13', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(932, 4776, '2020-02-20', '15:57:24', 'Ilham', 'Karcis', 20, 100000, 0, ''),
(933, 4777, '2020-02-20', '15:58:40', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(934, 4778, '2020-02-20', '15:58:47', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(935, 4978, '2020-02-20', '15:59:11', 'Ilham', 'Karcis', 200, 1000000, 0, 'Rombongan medan'),
(936, 4979, '2020-02-20', '15:59:27', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(937, 0, '2020-02-20', '15:59:33', 'Ilham Ramadhan', 'Setoran', 0, 0, 1285000, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(938, 4980, '2020-02-20', '16:00:22', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(939, 4981, '2020-02-20', '16:00:29', 'Ilham', 'Karcis', 1, 10000, 0, 'Single Karcis'),
(940, 4982, '2020-02-20', '16:00:35', 'Ilham', 'Karcis', 1, 10000, 0, 'Single Karcis'),
(941, 4983, '2020-02-20', '16:01:00', 'Ilham', 'Karcis', 1, 10000, 0, 'Single Karcis'),
(942, 4984, '2020-02-20', '16:01:06', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(943, 4985, '2020-02-20', '16:01:14', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(944, 4986, '2020-02-20', '16:09:06', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(945, 5186, '2020-02-20', '16:09:47', 'Ilham', 'Karcis', 200, 1000000, 0, 'Rombongan kuala simpang'),
(946, 0, '2020-02-20', '16:11:00', 'Ilham Ramadhan', 'Setoran', 0, 0, 1050000, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(947, 5187, '2020-02-20', '16:11:37', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(948, 5188, '2020-02-20', '16:33:49', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(949, 5189, '2020-02-20', '16:34:38', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(950, 5389, '2020-02-20', '16:35:03', 'Ilham', 'Karcis', 200, 1000000, 0, 'Rombongan medan'),
(951, 5390, '2020-02-20', '16:36:57', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(952, 5391, '2020-02-20', '16:37:07', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis'),
(953, 0, '2020-02-20', '16:39:26', 'Ilham Ramadhan', 'Setoran', 0, 0, 1025000, 'SETORAN DIBAYAR KE PERUSAHAAN'),
(954, 5392, '2020-02-20', '16:46:51', 'Ilham', 'Karcis', 1, 5000, 0, 'Single Karcis');

-- --------------------------------------------------------

--
-- Table structure for table `mangroove_officer`
--

CREATE TABLE `mangroove_officer` (
  `username` varchar(10) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `kelamin` varchar(10) NOT NULL,
  `jabatan` varchar(30) NOT NULL,
  `hp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mangroove_officer`
--

INSERT INTO `mangroove_officer` (`username`, `nama_lengkap`, `kelamin`, `jabatan`, `hp`) VALUES
('petugas1', 'Ilham', 'Laki-laki', 'Petugas Karcis', '085361885100');

-- --------------------------------------------------------

--
-- Table structure for table `mangroove_tarif`
--

CREATE TABLE `mangroove_tarif` (
  `no` int(11) NOT NULL,
  `karcis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mangroove_tarif`
--

INSERT INTO `mangroove_tarif` (`no`, `karcis`) VALUES
(1, 5000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mangroove_income`
--
ALTER TABLE `mangroove_income`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `mangroove_officer`
--
ALTER TABLE `mangroove_officer`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `mangroove_tarif`
--
ALTER TABLE `mangroove_tarif`
  ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mangroove_income`
--
ALTER TABLE `mangroove_income`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=955;

--
-- AUTO_INCREMENT for table `mangroove_tarif`
--
ALTER TABLE `mangroove_tarif`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
