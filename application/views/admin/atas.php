<div class="container">
    <div class="navbar-header">
        <!-- start: RESPONSIVE MENU TOGGLER -->
        <!--<button data-target="navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
        <span class="clip-list-2"></span>
    </button> -->
        <!-- end: RESPONSIVE MENU TOGGLER -->
        <!-- start: LOGO -->
        <br/>
         <h3 align="center" class="navbar"><font color="#5f5959" face="Century Gothic"><?php echo $header ?></font></h3>
        <!-- end: LOGO -->
    </div>
    <div class="navbar-tools">
        <!-- start: TOP NAVIGATION MENU -->
        <ul class="nav navbar-right hidden-xs hidden-md">
            <li class="dropdown current-user">
                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                    <img src="<?php echo base_url(); ?>vendor/assets/images/user.png" class="circle-img" alt="">
                    <span class="username"><?php echo $name; ?></span>
                    <i class="clip-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo base_url();?>index.php/web/logout/">
                                <i class="clip-exit"></i> &nbsp;Log Out
                            </a>
                        </li>
               		</li>
                </ul>
            </li>
        </ul>
        <!-- end: TOP NAVIGATION MENU -->
    </div>
</div>